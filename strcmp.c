#include <stdio.h>
#include <locale.h>

int main() {
	
	setlocale (LC_ALL, "Portuguese"); // adapta o programa ao idioma desejado.
	char pal1[100], pal2[100];
	
	int i = 0;
		
	printf ("Digite a primeira palavra: ");
	scanf ("%s", pal1);
	printf ("Digite a segunda palavra: ");
	scanf ("%s", pal2);
	
	while (pal1[i] != '\0' && pal2[i] != '\0') {
		if (pal1[i] < pal2[i]) {
			printf ("Palavra 2 e maior que a palavra 1\n");
			break;
		}
		else if (pal1[i] > pal2[i]) {
			printf ("Palavra 1 e maior que a palavra 2\n");
			break;
		}
		else {
			printf ("Ambas tem o mesmo tamanho!!");
			break;
		}
		i++;
		}
	}